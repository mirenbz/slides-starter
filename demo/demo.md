---
title: UNIT 0<br />Unit title
subtitle: The unit subtitle, if needed
theme: mbzdeusto
revealjs-url: ../assets/reveal.js
center: true
foot_left: "[CLASS] UNIT 0 and title"
css: style.css
color: "#CA694A"
noborder: true # to remove colored borders
...

# First level slide

## {.center}

Lesser level slides should always be `##`. Here, no heading and centered content.

:::notes
Speaker notes example.
:::

## Example of video embed

Some text.

<div class="center">

<iframe width="650" height="340" src="https://www.youtube.com/embed/9hIQjrMHTv4?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

</div>

## Example of quote

>A system of Internet servers that support specially formatted documents. The documents are formatted in a markup language called HTML \(_HyperText Markup Language_\) that supports links to other documents, as well as graphics, audio, and video files. This means you can jump from one document to another simply by clicking on hot spots. 
>
>There are several applications called Web browsers that make it easy to access the World Wide Web.
>
>_World Wide Web_ is **not** synonymous with the Internet.

<figure style="text-align:right;"><figcaption>
from [Webopedia](http://www.webopedia.com/TERM/W/World_Wide_Web.html)
</figcaption></figure>


## Slide with smaller text {.small}

This slide has the `.small` class, so text appears smaller

# Another section

## {.center}

Example of picture embed.

![Jennifer NIEDERST ROBBINS, _Learning Web Design_. O’Reilly: Canada, 2007. 681.374.13/N49j](https://mirenbz.gitlab.io/MSGW/unit1/img/process.png){width=550}

## {data-transition="slide-in fade-out"}

Modify transitions example

## {data-transition="fade"}

The next one, in the middle

## {data-transition="fade-in slide-out"}

Last one, slide out

### Example with smaller fragments

Some text

<div class="small">
Some smaller text
</div>
