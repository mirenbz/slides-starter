# Slides starter

This is a starter repo for creating slides.

- Fork this repo
- To build slides locally,
	- Run `git submodule update --init`
	- Add `mbzdeusto.revealjs` to `/usr/share/pandoc/data/templates`
	- Add `Pandoc -- Markdown to slides reveal.js [MBZDeusto].sublime-build` to Sublime Packages/User folder

Slide file YAML header:

```
---
title: UNIT 0<br />Unit title
subtitle: The unit subtitle, if needed
theme: mbzdeusto
revealjs-url: ../assets/reveal.js
center: true
foot_left: "[CLASS] UNIT 0 and title"
css: style.css
color: "#CA694A" 
...
```
